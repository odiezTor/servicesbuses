package org.buses.services.api.implementinterfaces;

import org.buses.services.api.beans.RespuestaBean;
import org.buses.services.api.dao.ListasAppIDAO;
import org.buses.services.api.interfaces.ListasAppI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("listasAppService")
public class ListasAppImpl implements ListasAppI {

	@Autowired
	private ListasAppIDAO listasAppIdao;
	
	public RespuestaBean getEstados() {
		// TODO Auto-generated method stub
		return listasAppIdao.getEstados();
	}

}
