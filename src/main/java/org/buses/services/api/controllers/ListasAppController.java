package org.buses.services.api.controllers;

import org.buses.services.api.beans.RespuestaBean;
import org.buses.services.api.interfaces.ListasAppI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/listaApp")
public class ListasAppController {
	
	@Autowired
	ListasAppI listaAppI;
	
	@RequestMapping(value="/getEstados", method=RequestMethod.GET)
	public @ResponseBody RespuestaBean getEstado(){
		
		return listaAppI.getEstados();
	}
}
