package org.buses.services.api.interfaces;

import org.buses.services.api.beans.RespuestaBean;

public interface ListasAppI {
	
	public RespuestaBean getEstados();

}
