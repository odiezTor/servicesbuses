package org.buses.services.api.beans;

public class RespuestaBean {
	private String error;
	private Object data;
	
	public RespuestaBean(){
		super();
	}
	
	public RespuestaBean(String error, Object data){
		super();
		this.error = error;
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	
	
	
}
