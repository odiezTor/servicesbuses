package org.buses.services.api.beans;

public class EstadoBean  {

	private int id_estado;
	private int clave_estado;
	private String nombre_estado;
	private String abreviatura;
	
	public EstadoBean(){
		super();
	}
	
	public EstadoBean(int idEstado, int claveEstado, String nombreEstado, String abreviatura){
		this.id_estado=idEstado;
		this.clave_estado=claveEstado;
		this.nombre_estado=nombreEstado;
		this.abreviatura = abreviatura;
	}

	
	public int getId_estado() {
		return id_estado;
	}

	public void setId_estado(int id_estado) {
		this.id_estado = id_estado;
	}

	public int getClave_estado() {
		return clave_estado;
	}

	public void setClave_estado(int clave_estado) {
		this.clave_estado = clave_estado;
	}

	public String getNombre_estado() {
		return nombre_estado;
	}

	public void setNombre_estado(String nombre_estado) {
		this.nombre_estado = nombre_estado;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
}
