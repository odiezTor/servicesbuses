package org.buses.services.api.dao.implement;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.buses.services.api.beans.EstadoBean;
import org.buses.services.api.beans.RespuestaBean;
import org.buses.services.api.dao.ListasAppIDAO;
import org.buses.services.util.HelperSqlSessionDao;
import org.springframework.stereotype.Repository;

@Repository("listasAppIDao")
public class ListasAppIDAOImpl extends HelperSqlSessionDao implements ListasAppIDAO {
	
	private final static String NAME_SPACE_AGTPROM = "org.buses.services.mapper.listasapp";
	static final Logger logger = Logger.getLogger(ListasAppIDAOImpl.class);

	public RespuestaBean getEstados() {
		// TODO Auto-generated method stub
		List<EstadoBean> estados = new ArrayList<EstadoBean>();
		RespuestaBean respB = new RespuestaBean();
		
		try{
			estados = getSqlSession().selectList(NAME_SPACE_AGTPROM.concat(".getEstados"));
			respB.setData(estados);
		}catch(Exception e){
			e.printStackTrace();
			respB.setError("Ocurrio un error al intenter obtener los estados.");
		}
		
		
		return respB;
	}

}
