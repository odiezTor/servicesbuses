package org.buses.services.api.dao;

import org.buses.services.api.beans.RespuestaBean;

public interface ListasAppIDAO {
	public RespuestaBean getEstados();
}
